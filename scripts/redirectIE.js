// Copyright (c) 2022 Aiden Baker
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

if(window.document.documentMode){window.location.replace("https://www.aspy.dev/internal/IE.html")};