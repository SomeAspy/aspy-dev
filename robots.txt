# Copyright (c) 2022 Aiden Baker
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

User-agent: *
Disallow: /external/
Disallow: /internal/
Disallow: /*.xml
Disallow: /*.txt
Disallow: /*.md
Disallow: /.vscode/
Sitemap: https://aspy.dev/sitemap.xml